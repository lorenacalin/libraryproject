import exceptions.InvalidBookIdentifier;

import java.util.LinkedHashSet;
import java.util.Set;

public class Book extends Publication{
    private LinkedHashSet<Author> authors;
    private String isbn;

    public Book(String title, LinkedHashSet<Author> authors, Genre genre, String isbn) throws InvalidBookIdentifier {
        super(title, genre);
        this.authors = authors;
        //ISBN - a 13 digit ISBN is valid (no matter how many "-" it has)
        isbn = isbn.replace("-","");
        if(!isbn.matches("([0-9]{13})")){
            throw new InvalidBookIdentifier("Invalid ISBN " + isbn);
        }
        this.isbn = isbn;
    }

    @Override
    public String getUniqueId() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LinkedHashSet<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(LinkedHashSet<Author> authors) {
        this.authors = authors;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return "Book{" +
                " isbn='" + isbn + '\'' +
                ", title='" + title + '\'' +
                ", genre=" + genre +
                '}';
    }
}
