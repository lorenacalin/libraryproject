public enum Genre {
    FANTASY,
    SELF_HELP,
    SCIENCE,
    COMPUTER_SCIENCE,
    BIOLOGY,
    PSYCHOLOGY
}
