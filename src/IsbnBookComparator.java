import java.util.Comparator;

public class IsbnBookComparator implements Comparator<Book> {

    @Override
    public int compare(Book b1, Book b2) {
        return b1.getUniqueId().compareTo(b2.getUniqueId());
    }
}
