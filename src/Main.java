import exceptions.InvalidArticleIdentifier;
import exceptions.InvalidBookIdentifier;
import exceptions.InvalidMovieIdentifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;

public class Main {
    public static void main(String[] args) throws IOException, InvalidMovieIdentifier, InvalidBookIdentifier, InvalidArticleIdentifier {
        Storage storage = new Storage();
        ProductReader.readProductsFromFile(storage);

        System.out.println(storage.getBookByTitle("Alba ca zapada"));
        System.out.println(storage.getBookByTitle("Eat, pray, love"));

        ArrayList<Book> booksOfDanBrown = new ArrayList<>();
        booksOfDanBrown = storage.getAllBooksOfAuthor("Dan Brown");
        System.out.println("Dan Brown's books: ");
        booksOfDanBrown.forEach(book ->
        {
            System.out.println(book.getTitle());
        });

        storage.booksStorage.forEach(System.out::println);
    }
}
