import exceptions.InvalidMovieIdentifier;

import java.util.LinkedHashSet;

public class Movie extends Publication {
    private LinkedHashSet<Actor> actors;
    private String imdb_id;

    public Movie(String title, Genre genre, LinkedHashSet<Actor> actors, String imdb_id) throws InvalidMovieIdentifier {
        super(title, genre);
        this.actors = actors;
        if(!imdb_id.matches("[a-zA-Z0-9]+")){
            throw new InvalidMovieIdentifier("IMDB id " + imdb_id + " contains invalid characters.");
        }
        this.imdb_id = imdb_id;
    }

    @Override
    public String getUniqueId() {
        return imdb_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public LinkedHashSet<Actor> getActors() {
        return actors;
    }

    public void setActors(LinkedHashSet actors) {
        this.actors = actors;
    }

    public void setImdb_id(String imdb_id) {
        this.imdb_id = imdb_id;
    }

    @Override
    public String toString() {
        return "Movie{" +
                ", imdb_id='" + imdb_id + '\'' +
                ", title='" + title + '\'' +
                ", genre=" + genre +
                '}';
    }
}
