import exceptions.InvalidArticleIdentifier;
import exceptions.InvalidBookIdentifier;
import exceptions.InvalidMovieIdentifier;

import java.io.*;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;

public class ProductReader {
    public static void readProductsFromFile(Storage storage) throws IOException, InvalidMovieIdentifier, InvalidBookIdentifier, InvalidArticleIdentifier {
        BufferedReader reader = new BufferedReader(new FileReader("products.txt"));

//        Set<Book> books = new LinkedHashSet<>();
//        Set<ScientificArticle> scientificArticles = new LinkedHashSet<>();
//        Set<Movie> movies = new LinkedHashSet<>();

        String line;
        while ((line = reader.readLine()) != null) {
            String[] infos = line.split("~");
            if (infos[0].equals("B")) {
                String title = infos[1];
                String allAuthors = infos[2];
                String[] authorNames = allAuthors.split("-");
                LinkedHashSet<Author> authors = new LinkedHashSet<>();
                for (int i = 0; i < authorNames.length; i++) {
                    Author a = new Author(authorNames[i]);
                    authors.add(a);
                    storage.allAuthors.add(a);
                }
                Genre genre = Genre.valueOf(infos[3]);
                String isbn = infos[4];

                Book book = new Book(title, authors, genre, isbn);
                storage.booksStorage.add(book);
            } else if (infos[0].equals("A")) {
                String title = infos[1];
                String allAuthors = infos[2];
                String[] authorNames = allAuthors.split("-");
                LinkedHashSet<Author> authors = new LinkedHashSet<>();
                for (int i = 0; i < authorNames.length; i++) {
                    Author a = new Author(authorNames[i]);
                    authors.add(a);
                    storage.allAuthors.add(a);
                }
                Genre theme = Genre.valueOf(infos[3]);
                String doi = infos[4];

                ScientificArticle scientificArticle = new ScientificArticle(title,authors, theme, doi);
                storage.articlesStorage.add(scientificArticle);
            } else if(infos[0].equals("M")){
                String title = infos[1];
                String allAuthors = infos[3];
                String[] actorNames = allAuthors.split("-");
                LinkedHashSet<Actor> actors = new LinkedHashSet<>();
                for (int i = 0; i < actorNames.length; i++) {
                    Actor a = new Actor(actorNames[i]);
                    actors.add(a);
                    storage.allActors.add(a);
                }
                Genre genre = Genre.valueOf(infos[2]);
                String imdb_id = infos[4];

                Movie movie = new Movie(title, genre, actors, imdb_id);
                storage.moviesStorage.add(movie);
            }

        }

    }
}
