import java.util.Objects;

public abstract class Publication {
    protected String title;
    protected Genre genre;

    public Publication(String title, Genre genre) {
        this.title = title;
        this.genre = genre;
    }

    public abstract String getUniqueId();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publication that = (Publication) o;
        return Objects.equals(getUniqueId(), that.getUniqueId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUniqueId());
    }
}
