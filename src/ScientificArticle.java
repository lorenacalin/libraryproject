import exceptions.InvalidArticleIdentifier;

import java.util.LinkedHashSet;

public class ScientificArticle extends Publication {
    private LinkedHashSet<Author> authors;
    private String doi;

    public ScientificArticle(String title, LinkedHashSet<Author> authors, Genre genre, String doi) throws InvalidArticleIdentifier {
        super(title, genre);
//        if(!doi.matches("([a-zA-Z0-9]+)|(^https://[a-zA-Z0-9])")){
//            throw new InvalidArticleIdentifier("Invalid DOI " + doi);
//        }
        this.doi = doi;
    }

    public LinkedHashSet<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(LinkedHashSet<Author> authors) {
        this.authors = authors;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    @Override
    public String getUniqueId() {
        return doi;
    }

    @Override
    public String toString() {
        return "ScientificArticle{" +
                "title='" + title + '\'' +
                ", genre=" + genre +
                ", doi='" + doi + '\'' +
                '}';
    }
}
