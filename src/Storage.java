import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class Storage {
    public TreeSet<Book> booksStorage;
    public LinkedHashSet<ScientificArticle> articlesStorage;
    public LinkedHashSet<Movie> moviesStorage;
    public LinkedHashSet<Author> allAuthors;
    public LinkedHashSet<Actor> allActors;

    public Storage() {
        this.booksStorage = new TreeSet<>(new IsbnBookComparator());
        this.articlesStorage = new LinkedHashSet<>();
        this.moviesStorage = new LinkedHashSet<>();
        this.allActors = new LinkedHashSet<>();
        this.allAuthors = new LinkedHashSet<>();
    }

    public Book getBookByTitle(String title) {
        for (Book book : booksStorage) {
            if (book.getTitle().equals(title)) {
                return book;
            }
        }
        System.out.println("No book with title : " + title);
        return null;
    }


    public ArrayList<Book> getAllBooksOfAuthor(String authorName) {
        ArrayList<Book> allBooks = new ArrayList<>();
        for (Book book : booksStorage) {
            for (Author author : book.getAuthors()) {
                if (author.getName().equals(authorName)) {
                    allBooks.add(book);
                }
            }
        }
        return allBooks;
    }
}
