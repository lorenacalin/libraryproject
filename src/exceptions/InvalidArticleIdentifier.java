package exceptions;

public class InvalidArticleIdentifier extends Exception{
    public InvalidArticleIdentifier(String message){
        super(message + " The DOI must be either an alphanumeric string or as a webpage URL");
    }
}
