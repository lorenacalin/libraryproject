package exceptions;

public class InvalidBookIdentifier extends Exception{

    public InvalidBookIdentifier(String message){
        super(message + " The ISBN id must contain 13 digits and a variable number of hyphens.");
    }
}
