package exceptions;

public class InvalidMovieIdentifier extends Exception{
    public InvalidMovieIdentifier(String message){
        super(message + " IMDB ids must only contain alphanumerical characters.");
    }
}
